
package com.publicis.sapient;

import java.util.ArrayList;
import java.util.List;

import com.google.api.services.bigquery.model.TableFieldSchema;
import com.google.api.services.bigquery.model.TableReference;
import com.google.api.services.bigquery.model.TableRow;
import com.google.api.services.bigquery.model.TableSchema;
import com.google.cloud.dataflow.sdk.Pipeline;
import com.google.cloud.dataflow.sdk.io.BigQueryIO;
import com.google.cloud.dataflow.sdk.options.Default;
import com.google.cloud.dataflow.sdk.options.Description;
import com.google.cloud.dataflow.sdk.options.GcpOptions;
import com.google.cloud.dataflow.sdk.options.PipelineOptions;
import com.google.cloud.dataflow.sdk.options.PipelineOptionsFactory;
import com.google.cloud.dataflow.sdk.options.Validation;
import com.google.cloud.dataflow.sdk.transforms.DoFn;
import com.google.cloud.dataflow.sdk.transforms.GroupByKey;
import com.google.cloud.dataflow.sdk.transforms.PTransform;
import com.google.cloud.dataflow.sdk.transforms.ParDo;
import com.google.cloud.dataflow.sdk.values.KV;
import com.google.cloud.dataflow.sdk.values.PCollection;

public class StarterPipelineCloudSQL {

	public static final String WFP_VISITOR_TABLE = "wfp_poc_dataset.bq_visitors";
	public static final String WFP_VISITOR_TRANSFORMED_TABLE = "bq_visitors_transformed";
	
	public static void main(String[] args) {
		DatastoreOptions options = PipelineOptionsFactory.fromArgs(args).withValidation().as(DatastoreOptions.class);
		TableSchema schema = buildTransformedVisitorSchema();
		TableReference tableRef = new TableReference();
		tableRef.setDatasetId(options.as(DatastoreOptions.class).getOutputDataset());
		tableRef.setProjectId(options.as(GcpOptions.class).getProject());
		tableRef.setTableId(options.getOutputTableName());
		options.setTempLocation("gs://bigquery-temp-0412");
		Pipeline p = Pipeline.create(options);
		p.apply(BigQueryIO.Read.named("Read").fromQuery("SELECT DATE, United_Kingdom FROM [" + WFP_VISITOR_TABLE+"]")
				.withoutValidation())
		.apply(new GroupByDate())
		.apply(ParDo.named("sum").of(new SumOfVisitors()));
		//write to cloudsql instance
		p.run();

	}

	/** Helper method to build the table schema for the output table. */
	private static TableSchema buildTransformedVisitorSchema() {
		List<TableFieldSchema> fields = new ArrayList<>();
		fields.add(new TableFieldSchema().setName("DATE").setType("STRING"));
		fields.add(new TableFieldSchema().setName("United_Kingdom").setType("INTEGER"));
		return new TableSchema().setFields(fields);
	}

	static class GroupByDate extends PTransform<PCollection<TableRow>, PCollection<KV<Integer, Iterable<TableRow>>>> {
		private static final long serialVersionUID = 1L;

		@Override
		public PCollection<KV<Integer, Iterable<TableRow>>> apply(PCollection<TableRow> rows) {
			PCollection<KV<Integer, TableRow>> kvs = rows.apply(ParDo.of(new TransferKeyValueFn()));
			PCollection<KV<Integer, Iterable<TableRow>>> group = kvs.apply(GroupByKey.<Integer, TableRow>create());
			return group;
		}
	}

	interface DatastoreOptions extends PipelineOptions {

		@Description("Path of the bigquery table to read from")
		@Default.String("wfp_poc_dataset.bq_visitors")
		String getInputTable();

		void setInputTable(String value);

		@Default.String(StarterPipelineBigQuery.WFP_VISITOR_TRANSFORMED_TABLE)
		String getOutputTableName();

		void setOutputTableName(String value);

		@Description("Table to read from, specified as " + "<project_id>:<dataset_id>.<table_id>")
		@Default.String(StarterPipelineBigQuery.WFP_VISITOR_TABLE)
		String getInput();

		void setInput(String value);

		@Description("Table to write to, specified as " + "<project_id>:<dataset_id>.<table_id>. "
				+ "The dataset_id must already exist")
		@Default.String(StarterPipelineBigQuery.WFP_VISITOR_TRANSFORMED_TABLE)
		@Validation.Required
		String getOutput();

		void setOutput(String value);

		@Description("Output destination Datastore ProjectID")
		@Default.String("gcp-tensorflow-poc")
		String getOutputProjectID();

		void setOutputProjectID(String value);

		@Default.String("wfp_poc_dataset")
		String getOutputDataset();

		void setOutputDataset(String value);

	}

}

class KeyValueFn extends DoFn<TableRow, KV<Integer, TableRow>> {

	private static final long serialVersionUID = 1L;

	public void processElement(ProcessContext c) {
		TableRow row = c.element();
		c.output(KV.of(Integer.parseInt(row.get("DATE").toString().split("Q")[0]), row));
	}

}

class SumOfVisitors extends DoFn<KV<Integer, Iterable<TableRow>>, TableRow> {

	private static final long serialVersionUID = 1L;

	@Override
	public void processElement(ProcessContext c) throws Exception {
		long sum = 0;
		for (TableRow row : c.element().getValue()) {
			sum += Integer.valueOf((String) row.get("United_Kingdom"));
		}
		TableRow row = new TableRow();
		row.set("DATE", c.element().getKey().toString().split("Q")[0]);
		row.set("United_Kingdom", sum);
		c.output(row);	
	}

}


